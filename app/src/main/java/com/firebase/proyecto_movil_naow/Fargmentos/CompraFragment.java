package com.firebase.proyecto_movil_naow.Fargmentos;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.proyecto_movil_naow.R;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.pdf.PdfWriter;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;

import static android.content.ContentValues.TAG;


public class CompraFragment extends Fragment {
    private SeekBar Alcalina, AguaGas, AguaSinGas, Smart, Energy;
    private TextView AlcalinaD, AguaGasD, AguaSinGasD, SmartD, EnergyD, Datos;
    private Button Comprar;
    private String result = "";
    final private int REQUEST_CODE_ASK_PERMISSIONS=111;
    private File pdfFile;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_compra, container, false);

        Alcalina = (SeekBar)view.findViewById(R.id.SeekbarAgua);
        AguaGas = (SeekBar)view.findViewById(R.id.SeekbarAguaGas);
        AguaSinGas = (SeekBar)view.findViewById(R.id.SeekbarAguaMesa);
        Smart = (SeekBar)view.findViewById(R.id.SeekbarSmart);
        Energy = (SeekBar)view.findViewById(R.id.SeekbarEnergy);

        AlcalinaD = (TextView)view.findViewById(R.id.LBLAgua);
        AguaGasD = (TextView)view.findViewById(R.id.LBLAguaGas);
        AguaSinGasD = (TextView)view.findViewById(R.id.LBLAguaMesa);
        SmartD = (TextView)view.findViewById(R.id.LBLSmart);
        EnergyD = (TextView)view.findViewById(R.id.LBLEneregy);
        Datos = (TextView)view.findViewById(R.id.LBLCOnsumo);

        Comprar = (Button)view.findViewById(R.id.BTNCarrito);

        Comprar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //precios
                Double a = 8.00;
                Double b = 6.50;
                Double c = 6.50;
                Double d = 9.00;
                Double e = 6.50;

                //productos
                Integer AL = Integer.valueOf(AlcalinaD.getText().toString());
                Integer AM = Integer.valueOf(AguaSinGasD.getText().toString());
                Integer AMG = Integer.valueOf(AguaGasD.getText().toString());
                Integer SM = Integer.valueOf(SmartD.getText().toString());
                Integer EN = Integer.valueOf(EnergyD.getText().toString());

                //Total x producto
                Double SAL = a*AL;
                Double SAM = b*AM;
                Double SAMG = c*AMG;
                Double SSM = d*SM;
                Double SEN = e*EN;

                //TOTAL
                Double TOTAL = SAL + SAM + SAMG + SSM + SEN;


                String probando = AL +      " AGUA ALCALINA" +  "                   " + a + "  " + SAL
                               +"\n" + AM + " AGUA DE MESA" +  "                     " + b + "  " + SAM +
                                "\n"+ AMG+  " AGUA CON GAS" +  "                    " + c + "  " +  SAMG
                              + "\n"+ SM +  " SMART DRINK" +  "                       " + d + "  " + SSM +
                                "\n"+ EN +  " ENERGY DRINK" +  "                      " + e + "  " + SEN +
                                "\nTOTAL" +"                                                  "+ TOTAL;
               Datos.setText(probando);

            }
        });


        Alcalina.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                AlcalinaD.setText(String.valueOf(progress));

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        AguaGas.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                AguaGasD.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        AguaSinGas.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                AguaSinGasD.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Smart.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                SmartD.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });

        Energy.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                EnergyD.setText(String.valueOf(progress));
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }
        });



        return view;
    }
}
